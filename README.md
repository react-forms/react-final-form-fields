# 🏁 React Final Form Fields

## What it is

A list of great batteries-included forms to toss into `react-final-form`. It exports:

```js
{
  Checkbox,
  TextField,
  Textarea,
  TextFieldArray,
  Select,
  Radio,
  Tags,
  validate // for validation
}
```

## Usage

