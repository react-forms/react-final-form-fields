// @flow

import React from 'react'

type Props = {
  children: React$Element,
  style?: object,
  className?: string,
  onClick?: func
};

const Row = ({ children, className, style }: Props) => {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'row',
        ...style
      }}
      className={className}
    >
      {children}
    </div>
  )
}

const Column = ({ onClick, children, className, style }: Props) => {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        ...style
      }}
      className={className}
      onClick={onClick}
    >
      {children}
    </div>
  )
}

export { Row, Column }
