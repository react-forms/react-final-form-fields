// @flow

import React from 'react'

type Props = {
  children: any,
  noSubmit?: boolean,
  onClick?: Function,
  small?: boolean,
  to: any,
  primary?: boolean,
  className?: string,
  as?: string,
  style?: Object,
  newTab?: boolean,
  column?: boolean
};

const classes = {
  button: {
    display: 'flex',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    height: 51,
    backgroundColor: 'transparent',
    border: 'none',
    color: '#437ABC',
    padding: '0px 10px',
    textDecoration: 'none',
    cursor: 'pointer',
    fontSize: '1em'
  }
}

classes.buttonSmall = {
  ...classes.button,
  padding: '2px 5px'
}

classes.btn = {
  ...classes.button,
  // display: 'initial'
}

classes.btnSmall = {
  ...classes.button,
  // display: 'initial'
}

const Button = (props: Props) => {
  const { style: customStyle, children, noSubmit, small, onClick, to, primary, className, newTab, column } = props
  const style = !small ?
    { ...classes.button, ...customStyle }
  :
    { ...classes.buttonSmall, ...customStyle }

  if (noSubmit) {
    return (
      <input
        style={{
          display: "flex",
          alignItems: "center",
          alignContent: "center",
          justifyContent: "center",
          height: 51,
          backgroundColor: "transparent",
          border: "none",
          color: "#437ABC",
          padding: "0px 10px",
          cursor: "pointer",
          border: primary ? '1px solid #437ABC' : undefined,
          borderRadius: primary ? 7 : undefined
        }}
        type="button"
        value={children}
        onClick={onClick}
        primary={primary}
      />
    )
  }

  return (
    <button
      className={className}
      style={style}
      onClick={onClick}
    >
      {children}
    </button>
  )
}

export default Button
