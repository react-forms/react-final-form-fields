import validator from 'validator'

class Validate {
  constructor () {
    for (const method in validator) {
      this[method] = (value = '') => { // eslint-disable-line no-loop-func
        if (method === 'isPostalCode') {
          if (validator[method](value, 'CA')) {
            return undefined
          }
        } else if (method === 'isMobilePhone') {
          const sanitizedValue = value.match(/\d+/g).join('')
          if (validator[method](sanitizedValue, 'en-CA')) {
            return undefined
          }
        } else if (validator[method](value)) {
          return undefined
        }

        let error

        switch (method) {
          case 'isEmail':
            return 'Should be an email address.'

          case 'isPostalCode':
            return 'Should be a postal code.'

          case 'isMobilePhone':
            return 'Needs to be a phone number.'

          case 'isBefore':
            if (value.match(/\d+/g).join('').length < 8) {
              return 'Should be YYYY/MM/DD.'
            }
            return 'Should be a date before today.'

          default:
            return 'Wrong type of value, try again.'
        }

        return error
      }
    }

    this.isRequired = (value = '') => {
      if (!validator.isEmpty(value)) {
        return undefined
      }

      return 'This info is needed.'
    }
  }
}

const validate = new Validate()

export default validate
