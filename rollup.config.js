import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import babel from 'rollup-plugin-babel';
import {uglify} from 'rollup-plugin-uglify';

// `npm run build` -> `production` is true
// `npm run dev` -> `production` is false
const production = !process.env.ROLLUP_WATCH;

export default {
  format: 'umd',
  input: 'src/index.js',
  output: {
    name: 'dist',
    file: 'dist/index.js',
    format: 'umd', // immediately-invoked function expression — suitable for <script> tags
    sourcemap: true
  },
  external: [
    'react'
  ],
  plugins: [
    babel(),
    resolve(),
    commonjs()
    // production && uglify() // minify, but only in production
  ]
};